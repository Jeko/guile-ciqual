(use-modules
 (web client)
 (ice-9 receive)
 (ice-9 iconv)
 (json)
 (srfi srfi-1))

(define-json-type <component-request-match>
  (query)
  (size))

(define-json-type <request-match>
  (from)
  (size)
  (query)
  (sort))

(define-json-type <request-multi-match>
  (from)
  (size)
  (query)
  (_source))

(define-json-type <response-multi-match>
  (took)
  (timed_out)
  (_shards)
  (hits))

(define-json-type <response-match>
  (took)
  (timed_out)
  (_shards)
  (hits))

(define-json-type <hits>
  (hits)
  (max_score)
  (total))

(define (query-ciqual-constituants request)
  (receive (response-server response-body)
      (http-post
       "https://ciqual.anses.fr/esearch/constituants/_search"
       #:headers `((content-type . (application/x-www-form-urlencoded (charset . "utf-8"))))
       #:body request)
    (bytevector->string response-body "utf-8")))

(define (query-ciqual-ingredients request)
  (receive (response-server response-body)
      (http-post
       "https://ciqual.anses.fr/esearch/aliments/_search"
       #:headers `((content-type . (application/x-www-form-urlencoded (charset . "utf-8"))))
       #:body request)
    (bytevector->string response-body "utf-8")))

(define (component-request-match code)
  (json->component-request-match
   (simple-format
    #f
    "{
     \"size\": 1,
     \"query\": {
	       \"match_phrase\": {
				\"constCode\": \"~A\"
				}
	       }
     \"_source\": {
		 \"excludes\": [
			      \"alimentDtos.groupeAfficheEng\",
			      \"alimentDtos.code\",
			      \"alimentDtos.groupeAfficheFr\",
			      \"alimentDtos.nomSortEng\",
			      \"alimentDtos.nomSortFr\",
			      \"alimentDtos.compoSourceCode\",
			      \"alimentDtos.nomIndexFr\",
			      \"alimentDtos.nomIndexEng\",
			      \"alimentDtos.compoConstcode\",
			      \"alimentDtos.compoSourceLibelles\"
			      ]
		 }
     }" (number->string code))))

(define (component-request-multi-match label)
  (json->request-multi-match
   (simple-format #f "{
 \"from\": 0,
 \"size\": 5,
 \"query\": {
	   \"bool\": {
		    \"must\": [
			     {
			      \"match\": {
					\"constNomFr\": {
						       \"query\": \"~A\"
						       }
					}
			      }
			     ],
		    \"should\": [
			       {
				\"prefix\": {
					   \"constNomFr\": {
							  \"value\": \"~A\",
							  \"boost\": 2
							  }
					   }
				}
			       ]
		    }
	   },
 \"_source\": {
	     \"excludes\": [
			  \"alimentDtos\"
			  ]
	     }
 }" label label)))

(define (ingredient-request-match code)
  (json->request-match
   (simple-format #f "{
   \"from\": 0,
   \"size\": 10000,
   \"query\": {
	     \"match_phrase\": {
			      \"code\": {
				       \"query\": \"~A\"
				       }
			      }
	     },
   \"sort\": \"nomSortFr\"
   }" (number->string code))))

(define (request-multi-match label)
  (json->request-multi-match
   (simple-format #f "{
     \"from\": 0,
     \"size\": 5,
     \"query\": {
	       \"bool\": {
			\"must\": [
				 {
				  \"multi_match\": {
						  \"query\": \"~A\",
						  \"fields\": [
							     \"nomIndexFr^2\",
							     \"nomFr\"
							     ]
						  }
				  }
				 ],
			\"should\": [
				   {
				    \"prefix\": {
					       \"nomSortFr\": {
							     \"value\": \"~A\",
							     \"boost\": 2
							     }
					       }
				    }
				   ]
			}
	       },
     \"_source\": {
		 \"excludes\": [
			      \"compos\",
			      \"groupeAfficheEng\",
			      \"nomEng\",
			      \"nomSortEng\",
			      \"nomSortFr\",
			      \"nomIndexFr\",
			      \"nomIndexEng\"
			      ]
		 }
     }"
		  label label)))


;;;;;;;;;;;;;;;;;;;;;;;;
;; (define rm-tomate-crue (ingredient-request-match 20584))

;; (define rmm-tomate (request-multi-match "tomate"))

;; (define tomate-candidates
;;   (json->response-multi-match
;;    (query-ciqual-ingredients
;;     (request-multi-match->json
;;      (request-multi-match "tomate")))))

(define (candidates-for a-string)
  (json->response-multi-match
   (query-ciqual-ingredients
    (request-multi-match->json
     (request-multi-match a-string)))))

;; (define matching-ingredients (cdr (car (response-multi-match-hits tomate-candidates))))

;; (define ing0 (vector-ref matching-ingredients 0))

;; (define ing0-code (cdar (filter (lambda (elem) (string=? "code" (car elem))) (drop (car ing0) 1))))

;; (define tomate-crue
;;   (json->response-match (query-ciqual-ingredients (request-match->json rm-tomate-crue))))

;; (define tomate-crue-compo
;;   (cdr (list-ref (list-ref (vector-ref (cdr (car (response-match-hits tomate-crue))) 0) 1) 3)))

;; (define comp0
;;   (vector-ref tomate-crue-compo 0))

;; (display
;;  (list-ref
;;   (vector-ref
;;    (cdar
;;     (response-match-hits
;;      (json->response-match
;;       (query-ciqual-constituants
;;        (component-request-match->json (component-request-match 10150))))))
;;    0)
;;   0))
